
module PWM_Generator_Verilog
 (
 clk, 
 data_in,
 High,
 Period, 
 Res,
 PWM_OUT ,
 eroare
 );
 input clk;
 input [7:0]data_in;
 input High;
 input Period;
 input Res;
 output reg PWM_OUT;
 output reg eroare;
 reg [7:0] H_c;
 reg [7:0] H;
 reg [7:0] P_c;
 reg [7:0] P;
 reg H_flag, P_flag;
 reg Valid;
 reg [7:0] counter;
 
 always @(posedge clk)
 begin
	if(Res == 1)
	begin
		H_flag <= 0;
		P_flag <= 0;
		Valid <= 0;
	end
	else
	begin
		if (High == 1 && Period == 1)
		begin
			H_flag <= 0;
			P_flag <= 0;
		end
		else
			begin
			if (High == 1)
				begin
					H_c <= data_in;
					H_flag <= 1;
				end
			
		  else
			begin
			if (Period == 1)
				begin
					P_c <= data_in;
					P_flag <= 1;
				end
			end
		end
	end
	end
 
 
 always @(posedge clk)
 begin
	if (Res == 1 )
	  begin
		counter = 8'b0;
		PWM_OUT = 1'b0;
		eroare = 1'b0;
	 end
	else
	begin
		if ( (H_flag == 1 && P_flag == 1 && High ==0 && Period == 0) || (counter == 0 && H_flag == 1 && High ==0 && (Valid == 1 || eroare ==1)) || ((( eroare == 1 ||Valid == 1) && counter == 0 && Period == 0 && P_flag == 1)))
		  begin
		    P = P_c;
		    H = H_c; 
		    H_flag = 0;
		    P_flag = 0;
		    
		    if (H_c >= P_c)
		      begin
		          Valid = 0;
		         eroare = 1'b1;
		      end
		    else
		      begin
		        Valid = 1;
		        eroare = 1'b0;
		      end
		  end
		if (Valid == 1)
		begin
			if (counter >= P-1)
				counter = 8'b0;
			else
				counter = counter +1;
				
			if (counter < H)
				begin
					PWM_OUT = 1'b1;
				end
			else
				begin
					PWM_OUT = 1'b0;
				end
		end
		else
		  begin
		    if (eroare == 1)
		      begin
		          PWM_OUT = 1'b1;
		      end
		      else
		        begin
		  PWM_OUT = 1'b0;
		  counter = 8'b0;
		  end
		  end
	end
 end
 
endmodule 
